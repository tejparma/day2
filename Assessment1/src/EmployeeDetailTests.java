import static org.junit.Assert.assertEquals;
import java.io.IOException;
import java.util.List;
import org.junit.Test;

class EmployeeDetailTests {

	
	EmployeeDAO employeeDAO;
	List<EmployeeBean> list;
	List<EmployeeBean> expectList;

	@Test
	void getEmployeeTest() {
		employeeDAO = new EmployeeDAO();
		try {
			list = employeeDAO.readData();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(true, employeeDAO.getEmployee(102).equals(new EmployeeBean(102,"Sejpal",35000)));
	}
	
	void getCountTest() {
		employeeDAO = new EmployeeDAO();
		try {
			list = employeeDAO.readData();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(2, employeeDAO.getCount(list, 57000));
	}
	
}
