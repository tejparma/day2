import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Demo {
	public static void main(String[] args) {
		EmployeeDAO edao = new EmployeeDAO();
		List<EmployeeBean> list = new ArrayList<EmployeeBean>();
		try {
			list = edao.readData();
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
	
		for (EmployeeBean employeeBean : list) {

			System.out.println(employeeBean.toString());
		}
	}
}
